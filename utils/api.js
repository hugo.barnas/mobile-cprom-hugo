import axios from 'axios'
const api = axios.create({
    baseURL: 'https://api.pote.dev'
})

export default api