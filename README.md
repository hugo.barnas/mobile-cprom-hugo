# Mobile CPROM Hugo

Project : Build a mobil app with React Native App

1. Features of this app

- Register with a firstname, lastname, password and email
- Connected to my profil with my email and my password
- Reinitialization of my password by email
- Show my profil when I am connected
- Watching the list of users when I am connected

2. Wireframes et maquettes de l'application

    https://www.figma.com/file/4rza5668cVxrfXPxJQNM4K/Mobile-CPROM-Wireframes-et-maquettes
 

3. How to run the project ?
