import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import { View, TextInput, Button, StyleSheet, Text, TouchableOpacity } from 'react-native'
import api from "../utils/api";

export const Login = ({ navigation }) => {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [success, setSucess] = useState('')
    const [error, setError] = useState('')




    const handleSubmit = async (e) => {
        e.preventDefault()
        const body = {
            password,
            email,
        }

        try {
            const result = await api.post('auth/login', body);

            if (result.status === 201) {
                console.log(result)
            }
        } catch (err) {
            setError(err.response.data.message)
        }

    }

    return (
        <View style={styles.container}>
            <StatusBar style="auto" />
            <View style={{ flex: 5, padding: 10 }}>
                <Text style={styles.title}>
                    Login
                </Text>

                <TextInput
                    style={styles.input}
                    value={email}
                    placeholder="Insérer votre email"
                    onChangeText={(email) => setEmail(email)}
                />
                <TextInput
                    style={[styles.input, { marginBottom: 30 }]}
                    value={password}
                    placeholder="Insérer votre mot de passe"
                    onChangeText={(password) => setPassword(password)}
                />
                <TouchableOpacity style={styles.button}
                    onPress={handleSubmit}>
                    <Text style={{ fontSize: 30 }}>Se connecter</Text>
                </TouchableOpacity>
            </View>

            <View style={styles.footer}>
                <TouchableOpacity style={styles.login}
                    onPress={() =>
                        navigation.navigate('Login')}>
                    <Text style={{ color: "white", fontSize: 30 }}>
                        Login
                    </Text>

                </TouchableOpacity>
                <TouchableOpacity style={styles.register}
                    onPress={() =>
                        navigation.navigate('Register')}>
                    <Text style={{ color: "white", fontSize: 30 }}>
                        Register
                    </Text>
                </TouchableOpacity>
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginTop: 100,
        flex: 1,
        justifyContent: "center",
    },

    formulaire: {
        paddingHorizontal: 10,
        marginHorizontal: 10,
    },

    title: {
        marginTop: 20,
        justifyContent: "center",
        marginLeft: 10,
        marginBottom: 50,
        fontSize: 30
    },

    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
    },

    button: {
        marginTop: 70,
        alignItems: 'center',
        backgroundColor: '#DDDDDD',
        padding: 10,
    },

    footer: {
        flex: 1,
        flexDirection: "row",
        backgroundColor: "black",
    },

    login: {
        justifyContent: "center",
        alignItems: "center",
        flex: 1,
        color: 'white'

    },
    register: {
        justifyContent: "center",
        alignItems: 'center',
        flex: 1,
        color: 'white',
    }
})
export default Login